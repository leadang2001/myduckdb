import duckdb

# Connect to an existing database file
con = duckdb.connect('mydatabase.db')

# Creating a table with SQL
con.execute("CREATE TABLE mytable (id INTEGER PRIMARY KEY, name VARCHAR(50), age INTEGER)")

# Inserting data with SQL
con.execute("INSERT INTO mytable (id, name, age) VALUES (1, 'John', 30)")
con.execute("INSERT INTO mytable (id, name, age) VALUES (2, 'Jane', 25)")
con.execute("INSERT INTO mytable (id, name, age) VALUES (3, 'Bob', 40)")

# Querying data with SQL
result = con.execute("SELECT * FROM mytable")
for row in result.fetchall():
    print(row)

# Close the connection
con.close()